#!/bin/bash 
args=("$@")

PROJECT_ID=${args[0]}
CREATION_DATE=$(date -u +%Y-%m-%d)
gcloud config set project $PROJECT_ID

# ----------------------------------------
#       CREATE TEST TABLES
# ----------------------------------------
echo '--- INIT CREATE TEST TABLES ---'

TEST_NAME=datatest

bq --location=US mk \
    --dataset \
    --description description \
    ${PROJECT_ID}:${TEST_NAME}

DATASET_TEST=${PROJECT_ID}.${TEST_NAME}

bq query --nouse_legacy_sql \
"CREATE TABLE ${DATASET_TEST}.table_test
( 
id STRING NOT NULL,
name STRING NOT NULL,
);"

bq load \
    --noreplace \
    --allow_jagged_rows \
    --field_delimiter=',' \
    --skip_leading_rows=1 \
    --source_format=CSV \
    ${TEST_NAME}.table_test  \
    ./datatest.csv \
    ./schema_datatest.json

echo '--- END CREATE TEST TABLES ---'
