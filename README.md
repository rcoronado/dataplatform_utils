
# CDGP - CONFIGURACION EMPRESAS

## Autenticación en GCP

Previamente debe ingresar a la plataforma de datos y habilitar el proyecto en el que desea realizar la configuración. Previamente su cuenta de usuario debe tener permisos de **Owner** del proyecto, preferentemente.

## Proyecto Maestro - Data Control

Definimos una variable que contiene el nombre del proyecto **data control**

```bash
set | grep MY_PROJECT
MY_PROJECT=prd-data-control
echo $MY_PROJECT
```

Ejecutamos el script que configura el proyecto maestro.

- Crea la cuenta de servicio maestra.
- Crea el rol personalizado.
- Asigna los permisos a la cuenta de servicio.
- Habilita las apis de GCP.
- Crea los templates de DataCatalog.
- Crea tablas de mantenimiento en BigQuery

```bash
bash node_master.sh $MY_PROJECT
```

## Proyectos Nodo

Este paso sirve para los proyectos **storage-pub, storage-pv, sensitive, operational**.

Definimos una variable para el proyecto que deseamos configurar **MY_PROJECT** y una variable **MY_SA** que debe contener el nombre de la cuenta de servicio creada en el proyecto **data-control**

```bash
set | grep MY_PROJECT
MY_PROJECT=prd-data-storage
echo $MY_PROJECT

set | grep MY_SA
MY_SA=intercorp-dataplatform-sa@prd-data-control.iam.gserviceaccount.com
echo $MY_SA
```

Ejecutamos el script que configura el proyecto nodo designado.

- Crea el rol personalizado.
- Asigna los permisos a la cuenta de servicio.
- Habilita las apis de GCP.

```bash
bash node.sh $MY_PROJECT $MY_SA
```

## Tabla de Validación

Este paso solo debe ejecutarse para el proyecto **storage público**.

- Se creara un dataset llamado **datatest**
- Se creara una tabla llamada **table_test**
- Se insertaran registros de prueba.

```bash
set | grep MY_PROJECT
MY_PROJECT=prd-data-storage-pub
echo $MY_PROJECT

bash node_test_table.sh $MY_PROJECT
```

Finalmente debe verificar **IAM** de GCP para validar la asignación de los permisos a la cuenta de servicio. Sólo si ha configurado la cuenta maestra validar la creación de la plantillas en **DataCatalog**. Solo para el proyecto **storage público** validar la creación de la tabla de prueba.
