#!/bin/bash 
args=("$@")

PROJECT_ID=${args[0]}
CREATION_DATE=$(date -u +%Y-%m-%d)

echo "PROJECT_ID   ->"  $PROJECT_ID
echo "CREATION_DATE->"  $CREATION_DATE
echo 'SET PROJECT'
gcloud config set project $PROJECT_ID

# ---------------------------------------
#      CREATE MASTER SERVICE ACCOUNT
# ---------------------------------------
echo '--- INIT CREATE SERVICE ACCOUNT ---'

SA_ID='intercorp-dataplatform-sa'
SA_NAME=${SA_ID//-/_}
SA_ID_FULL=${SA_ID}@${PROJECT_ID}.iam.gserviceaccount.com
SA_DESCRIPTION="Cuenta de Servicio para Plataforma de Datos."

echo "SA_ID     ->"  $SA_ID
echo "SA_NAME   ->"  $SA_NAME
echo "SA_ID_FULL->" $SA_ID_FULL
echo "SA_DESCRIPTION->" $SA_DESCRIPTION

gcloud iam service-accounts create $SA_ID \
    --description="$SA_DESCRIPTION" \
    --display-name=$SA_NAME

echo '--- END CREATE SERVICE ACCOUNT ---'

# ----------------------------------------
#       ROLES AND PERMISSIONS
# ----------------------------------------

bash ./node.sh $PROJECT_ID $SA_ID_FULL

# ----------------------------------------
#       CREATE DATACATALOG TEMPLATE
# ----------------------------------------
echo '--- INIT CREATE DATACATALOG TEMPLATE ---'

# gcloud services enable datacatalog.googleapis.com

curl -X POST \
    -H "Authorization: Bearer "$(gcloud auth application-default print-access-token) \
    -H "Content-Type: application/json; charset=utf-8" \
    -d @tpl_tables.json \
    https://datacatalog.googleapis.com/v1beta1/projects/${PROJECT_ID}/locations/us-central1/tagTemplates?tagTemplateId=tpl_tables

curl -X POST \
    -H "Authorization: Bearer "$(gcloud auth application-default print-access-token) \
    -H "Content-Type: application/json; charset=utf-8" \
    -d @tpl_columns.json \
    https://datacatalog.googleapis.com/v1beta1/projects/${PROJECT_ID}/locations/us-central1/tagTemplates?tagTemplateId=tpl_columns

echo '--- END CREATE DATACATALOG TEMPLATE ---'


# ----------------------------------------
#       CREATE MAINTENANCE TABLES
# ----------------------------------------
echo '--- INIT CREATE MAINTENANCE TABLES ---'

LOG_NAME=DGP_default
DP_NAME=dataplatform

bq --location=US mk \
    --dataset \
    --description description \
    ${PROJECT_ID}:${LOG_NAME}

bq --location=US mk \
    --dataset \
    --description description \
    ${PROJECT_ID}:${DP_NAME}

DATASET_LOG=${PROJECT_ID}.${LOG_NAME}
DATASET_DP=${PROJECT_ID}.${DP_NAME}

bq query --nouse_legacy_sql \
"CREATE TABLE ${DATASET_LOG}.log_activity
( 
created TIMESTAMP NOT NULL,
log_type STRING NOT NULL,
activity STRING,
owner STRING,
applicant STRING,
object_id STRING,
object_name STRING,
response STRING,
description STRING,
operator STRING
);"

bq query --nouse_legacy_sql \
"CREATE TABLE ${DATASET_DP}.dp_area
( 
area_id INT64 NOT NULL,
area_description STRING,
start_date	DATETIME,
end_date	DATETIME,
flag_active	BOOL,
load_date	DATETIME,
record_source	STRING,
creator_user	STRING,
record_id INT64 
);"


bq query --nouse_legacy_sql \
"CREATE TABLE ${DATASET_DP}.dp_userprofile
( 
userprofile_id INT64 NOT NULL,
area_id INT64 NOT NULL,
userprofile_description STRING,
start_date	DATETIME,
end_date	DATETIME,
flag_active	BOOL,
load_date	DATETIME,
record_source	STRING,
creator_user	STRING,
record_id INT64 
);"

bq query --nouse_legacy_sql \
"CREATE TABLE ${DATASET_DP}.dp_userprofile_table
( 
userprofile_id INT64 NOT NULL,
table_id INT64 NOT NULL,
area_id INT64 NOT NULL,
project_name STRING,
dataset_name STRING,
table_name STRING,
accesstype_id INT64, --0=R, 1=R/W
default_accesstime_months INT64,
start_date	DATETIME,
end_date	DATETIME,
flag_active	BOOL,
load_date	DATETIME,
record_source	STRING,
creator_user	STRING,
record_id INT64 
);"

bq query --nouse_legacy_sql \
"CREATE TABLE ${DATASET_DP}.dp_profileassignment
( 
user_name STRING NOT NULL,
user_position STRING,
supervisor_email STRING,
applicant_email STRING,
userprofile_id INT64,
area_id INT64,
start_date	DATETIME,
end_date	DATETIME,
flag_active	BOOL,
load_date	DATETIME,
record_source	STRING,
creator_user	STRING,
record_id INT64 
);"

bq query --nouse_legacy_sql \
"CREATE TABLE ${DATASET_DP}.dp_loadlog
( 
load_date	DATETIME NOT NULL,
action STRING ,
affected_table STRING,
data_affected STRING,
record_source	STRING,
creator_user	STRING,
record_id INT64 
);"

bq query --nouse_legacy_sql \
"CREATE VIEW ${DATASET_DP}.v_list_dp_profileassigmment
AS 
SELECT a.area_id, c.area_description, a.userprofile_id, b.userprofile_description, a.user_name, a.user_position, a.supervisor_email, a.applicant_email,
project_name, dataset_name, table_name, accesstype_id, default_accesstime_months, a.flag_active	,
a.start_date, a.end_date, a.load_date, a.record_source, a.creator_user 
FROM ${DATASET_DP}.dp_profileassignment a INNER JOIN ${DATASET_DP}.dp_userprofile b
ON a.area_id=b.area_id and a.userprofile_id = b.userprofile_id INNER JOIN ${DATASET_DP}.dp_area c ON b.area_id = c.area_id 
INNER JOIN ${DATASET_DP}.dp_userprofile_table d ON b.area_id = d.area_id AND b.userprofile_id = d.userprofile_id
WHERE d.flag_active = true
ORDER BY 1, 3;"

echo '--- END CREATE MAINTENANCE TABLES ---'
