#!/bin/bash 
args=("$@")

PROJECT_ID=${args[0]}
SERVICE_ACCOUNT=${args[1]}

echo 'PROJECT_ID' $PROJECT_ID
echo 'SERVICE_ACCOUNT:' $SERVICE_ACCOUNT

gcloud config set project $PROJECT_ID

# --------------------------
#       CREATE ROLE
# --------------------------
echo '--- INIT CREATE ROLE ---'

CREATION_DATE=$(date -u +%Y-%m-%d)
ROLE_TITLE=${PROJECT_ID}_dgprole
ROLE_ID=${ROLE_TITLE//-/_}
ROLE_DESCRIPTION=${CREATION_DATE}
ROLE_PERMISSIONS=resourcemanager.projects.getIamPolicy,resourcemanager.projects.setIamPolicy,datacatalog.tagTemplates.update,bigquery.datasets.update,bigquery.tables.get

echo "PROJECT_ID->"  $PROJECT_ID
echo "ROLE_TITLE->"  $ROLE_TITLE
echo "ROLE_ID   ->" $ROLE_ID
echo "ROLE_DESCRIPTION->" $ROLE_DESCRIPTION

gcloud iam roles create $ROLE_ID --project=$PROJECT_ID \
  --title=$ROLE_TITLE --description="$ROLE_DESCRIPTION" \
  --permissions=$ROLE_PERMISSIONS --stage=ALPHA

echo '--- END CREATE ROLE ---'

# ----------------------------------
#      ADD ROLES AND PERMISSIONS
# ----------------------------------

echo '--- INIT ADD ROLES ---'

gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member serviceAccount:$SERVICE_ACCOUNT \
    --role roles/bigquery.user && \
gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member serviceAccount:$SERVICE_ACCOUNT \
    --role roles/bigquery.jobUser && \
gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member serviceAccount:$SERVICE_ACCOUNT \
    --role roles/bigquery.dataEditor && \
gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member serviceAccount:$SERVICE_ACCOUNT \
    --role roles/bigquery.metadataViewer && \
gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member serviceAccount:$SERVICE_ACCOUNT \
    --role roles/datacatalog.tagEditor && \
gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member serviceAccount:$SERVICE_ACCOUNT \
    --role roles/datacatalog.tagTemplateUser && \
gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member serviceAccount:$SERVICE_ACCOUNT \
    --role roles/datacatalog.tagTemplateViewer && \
gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member serviceAccount:$SERVICE_ACCOUNT \
    --role roles/datacatalog.entryViewer && \
gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member serviceAccount:$SERVICE_ACCOUNT \
    --role roles/datacatalog.entryGroupCreator && \
gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member serviceAccount:$SERVICE_ACCOUNT \
    --role roles/datacatalog.tagTemplateCreator && \
gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member serviceAccount:$SERVICE_ACCOUNT \
    --role projects/${PROJECT_ID}/roles/${ROLE_ID}

echo '--- END ADD ROLES ---'

# ----------------------------------
#      ENABLE GOOGLE CLOUD APIS
# ----------------------------------

echo '--- INIT ENABLE APIS ---'

gcloud services enable \
    bigquery.googleapis.com \
    datacatalog.googleapis.com \
    cloudresourcemanager.googleapis.com \
    iamcredentials.googleapis.com

echo '--- END ENABLE APIS ---'
